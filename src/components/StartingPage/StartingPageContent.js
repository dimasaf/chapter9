import classes from './StartingPageContent.module.css';

const StartingPageContent = () => {
  return (
    <section className={classes.starting}>
      <h1>Welcome Gamers!</h1>
      <p>Welcome to game Rock-Paper-Scissor.</p>
      <hr/>
    </section>
  );
};

export default StartingPageContent;
