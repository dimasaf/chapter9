import { createUserWithEmailAndPassword, getAuth } from "firebase/auth";
import { collection, getDocs } from "firebase/firestore";
import { useEffect, useState } from "react";
import { db } from "../firebase-config";
import RoomImage from "../assets/room-image.jpg";
import classes from "./Dashboard.module.css";

function ListUsers() {
  // List batch of users, 1000 at a time.
  const [allUser, setAllUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  function getUsers() {
    const user = collection(db, "users");
    getDocs(user)
      .then((response) => {
        const user = response.docs.map((doc) => ({
          data: doc.data(),
          id: doc.id,
        }));
        setAllUsers(user);
      })
      .catch((err) => console.warn(err.message));
  }

  return (
    <div>
      <div className={classes.row}>
        <div className={classes.card}>
          <a href="#">
            <img src={RoomImage} className={classes.cardImage} alt="..." />
          </a>
          <div className={classes.cardBody}>
            <p className={classes.cardTittle}>Gunting Batu Kertas</p>
            <p className={classes.cardDesc}>
              Bermain dan dapatkan poin saat menang
            </p>
            <a href="/" className={classes.cardButton}>
              Join
            </a>
          </div>
        </div>
        <div className={classes.card}>
          <a href="#">
            <img src={RoomImage} className={classes.cardImage} alt="..." />
          </a>
          <div className={classes.cardBody}>
            <p className={classes.cardTittle}>comming soon ...</p>
            <p className={classes.cardDesc}>game lainnya akan hadir</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ListUsers;
